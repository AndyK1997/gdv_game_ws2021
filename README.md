# Flight Game 
 
## Getting Started :rocket:

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

You need 16Gb diskspace , 8GB RAM and a CPU with 2 logic cores or better

#### Windows:
1. Installing Ubuntu Subsystem 

```
https://docs.microsoft.com/de-de/windows/wsl/install-win10
```


#### Linux/Debian:
1. Update Packages

```bash
$ sudo apt update && apt upgrade -y
```

### Installing

### Running and Stop after install

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [CMAKE](https://cmake.org/) - build tool
* [OpenGL](https://www.opengl.org//) -Graphic Interface
* [C++](https://www.cplusplus.com/) -Programming Language


## Authors

* **Andreas Kirchner** -[staskirc](https://gitlab.fbi.h-da.de/istaskirc)
* **Andreas Müller** -[staemue2](https://gitlab.fbi.h-da.de/istaemue2)

See also the list of [contributors](https://gitlab.fbi.h-da.de/gdv_game_ws2021/-/project_members) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Acknowledgments



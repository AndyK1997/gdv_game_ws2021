//Include OpenGL header files, so that we can use OpenGL
#ifdef __APPLE__
#include <OpenGL/OpenGL.h>
#include <GLUT/glut.h>
#else

#include <GL/glut.h>
#include "Plane.h"
#include "Environment.h"
#include "Challenge.h"

#endif

using namespace std;
bool start = true;
int counter = 0;
Challenge challenge;

// width and height of the window
GLsizei width = 1000;
GLsizei height = 1000;

enum {
    NIGHT_MODE,
    MENU_EXIT
};

/**
 * Initializes 3D rendering
 */
void initRendering() {
    Interface::cam_x = 0, Interface::cam_y = 8, Interface::cam_z = 40;
    challenge.createRandomNodes();
    // makes 3D drawing work when something is in front of something else
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_LIGHTING); //Enable lighting
    glEnable(GL_LIGHT0); //Enable light #0
    glEnable(GL_LIGHT1); //Enable light #1
    glEnable(GL_NORMALIZE); //Automatically normalize normals
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

/**
 * Here we define the cases in our menu
 */
void SelectFromMenu(int idCommand) {
    switch (idCommand) {
        case NIGHT_MODE:
            break;
        case MENU_EXIT:
            exit(0);
    }
    glutPostRedisplay();
}

/**
 * Here we build our pop up menu
 */
int BuildPopupMenu() {
    int menu;
    menu = glutCreateMenu(SelectFromMenu);
    glutAddMenuEntry("Night Mode\t", NIGHT_MODE);
    glutAddMenuEntry("Exit Demo\t", MENU_EXIT);
    return menu;
}

/**
 * Called when the window is resized
 */
void handleResize(int w, int h) {
    // tell OpenGL how to convert from coordinates to pixel values
    glViewport(0, 0, w, h);
    // switch to setting the camera perspective
    glMatrixMode(GL_PROJECTION);
    // reset the camera
    glLoadIdentity();
    // set the camera perspective
    gluPerspective(
            60.0f,                       /*The camera angle*/
            (double) w / (double) h,    /*The width-to-height ratio*/
            1.0f,                       /*The near z clipping coordinate*/
            10000.0f                       /*The far z clipping coordinate*/
    );
}

/**
 * Here we provide light effects for our scene
 */
void light() {
    //position light
    GLfloat light_position[] = {-1000, 5000.0, -10000.0, 0.0};
    GLfloat light_ambient[] = {0.9, 0.9, 0.9, 0};
    GLfloat mat_specular[] = {0.4, 0.4, 0.4, 0};
    GLfloat low_shininess[] = {20.0};
    //light position
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);
    //global ambient lightww
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, light_ambient);
    // reflections
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, mat_specular);
    glMaterialfv(GL_FRONT_FACE, GL_SPECULAR, mat_specular);
    glMaterialfv(GL_BACK_LEFT, GL_SPECULAR, mat_specular);
    glMaterialfv(GL_BACK_RIGHT, GL_SPECULAR, mat_specular);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, low_shininess);
    glMaterialfv(GL_FRONT_FACE, GL_SHININESS, low_shininess);
    glMaterialfv(GL_BACK_LEFT, GL_SHININESS, low_shininess);
    glMaterialfv(GL_BACK_RIGHT, GL_SHININESS, low_shininess);
}


/**
 * Here we build the graph for our scene
 */
void graph() {
    //hole graph
    glPushMatrix();

    //graph plane
    glPushMatrix();
    //std::cout<<"Plane x: "<<Interface::planeMoveX<<" Plane y: "<<Interface::planeMoveY<<" Plane z: "<<Interwwwwwface::planeMoveZ<<std::endl;
    glTranslatef(Interface::planeMoveX, Interface::planeMoveY, Interface::planeMoveZ);
    glRotatef(180, 0, 1, 0);
    glRotatef(Plane::roleDegree, 0, 0, 1);
    glRotatef(Plane::pitchDegree, 1, 0, 0);
    glRotatef(Plane::yawDegree, 0, 1, 0);

    //createField ground
    if (start) {
        Environment::setRandMapType();
        //Environment::createRandValuesForClouds();
        Environment::loadTextures();
        Plane::loadTextures();
        start = false;
    }

    //createField plane
    Plane::renderPlane();
    glPopMatrix();

    glPushMatrix();
    Environment::placeFields();
    glPopMatrix();

    glPushMatrix();
    Environment::renderClouds();
    glPopMatrix();

    glPushMatrix();
    Environment::renderSky();
    glPopMatrix();

    glPushMatrix();
    Environment::renderSun();
    glPopMatrix();

    //challenge nodes to follow
    challenge.createPath();

    glPopMatrix();
}

/**
 * Draws the 3D scene
 */
void drawScene() {
    // clear information from last draw
    glClearColor(0.0392, 0.584, 1.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT| GL_ACCUM_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    gluLookAt(Interface::planeMoveX + Interface::cam_x,
              Interface::planeMoveY + Interface::cam_y,
              Interface::planeMoveZ + Interface::cam_z,
              Interface::planeMoveX + Interface::position_x,
              Interface::planeMoveY + Interface::position_y,
              Interface::planeMoveZ + Interface::position_z,
              0.0f, 1.0f, 0.0f
    );

    //insert light effects
    light();

    //graph
    graph();

    glutSwapBuffers();

    Plane::checkAndSetValues();
    Plane::resetAngle();
    Plane::limitFieldPosition();
}

/**
 * Here we manipulate all elements in our graph to animate
 */
void update(int value) {
    if (counter == 60) {
        if (Plane::toggle) {
            Plane::toggle = false;
        } else {
            Plane::toggle = true;
        }
        counter = 0;
    }
    //Plane::fly();
    Plane::fly();
    //manipulate plane parts
    Interface::animate();
    // tell GLUT that the display has changed
    glutPostRedisplay();
    // tell GLUT to call update again in 25 milliseconds
    glutTimerFunc(1, update, value);
    counter++;
}

/**
 * main runner with endless loop
 */
int main(int argc, char **argv) {
    //initialize GLUT
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    //set the window size
    glutInitWindowSize(width, height);
    //create the window
    glutCreateWindow("Operation Flying Object");
    //initialize rendering
    initRendering();
    //set handler functions for drawing, keypress, and window resizes
    glutDisplayFunc(drawScene);
    glutMouseFunc(Interface::handleMouseButton);
    glutKeyboardFunc(Interface::handleKeyFunc);
    glutSpecialFunc(Interface::handleSpecialFunc);
    glutReshapeFunc(handleResize);
    //provide menu by clicking the mouse middle button
    BuildPopupMenu();
    glutAttachMenu(GLUT_MIDDLE_BUTTON);
    //sync animation
    glutTimerFunc(16, update, 0);
    // start the main loop
    glutMainLoop();
    // this line will never reached
    return 0;
}
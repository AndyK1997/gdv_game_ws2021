#ifndef OPERATION_FLYING_OBJECT_ENVIRONMENT_H
#define OPERATION_FLYING_OBJECT_ENVIRONMENT_H

#include <GL/glut.h>
#include "Interface.h"
#include <vector>

struct Cloud {
    int type;
    int high;
    int widthOffset;
    int deepOffset;
};

class Environment {
public:
    //texture loading
    static void loadTextures();
    static void pushTexture(const char *file);
    //create fields
    static void setRandMapType();
    static void createField(float xOld, float xNew, float y, float z, GLuint texture);
    static void createWood(float xOld, float xNew, float y, float z);
    static void createAcre(float xOld, float xNew, float y, float z);
    static void createCultivatedField(float xOld, float xNew, float y, float z);
    static void createMeadow(float xOld, float xNew, float y, float z);
    static void createSea(float xOld, float xNew, float y, float z);
    static void createCity(float xOld, float xNew, float y, float z);
    static void placeFields();
    //create parts
    static void renderSun();
    static void renderSky();
    static void renderClouds();
    static void createRandValuesForClouds();
    static void renderCloud(int xOld, int xNew, float y, int z, float deepness);
    static void renderTree();
    static void renderHouse(float x, float y, float z);
    static void renderRing();

    static int *arrayMapTypes;
    static Cloud *cloudTypes;

private:
    constexpr static const float groundX = 7000;
    constexpr static const float groundY = 1;
    constexpr static const float groundZ = 20000;
    constexpr static const float FIELDSIZE = 500;
};

#endif //OPERATION_FLYING_OBJECT_ENVIRONMENT_H

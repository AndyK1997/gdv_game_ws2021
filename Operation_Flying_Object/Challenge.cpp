#include "Challenge.h"

/**
 * Here we create all nodes for up coming challenge
 */
void Challenge::createRandomNodes() {
    srand((unsigned) time(nullptr));
    int counter = 0;

    while (counter != max_nodes) {
        int random_x = 1 + (rand() % 12);
        int random_y = 1 + (rand() % 12);
        int random_z = 1 + (rand() % 12);

        if (pos_x + randomValue[random_x] > -2000 && pos_x + randomValue[random_x] < 8000) {
            pos_x += randomValue[random_x];
        }
        if (pos_y + randomValue[random_y] < 1500 && pos_y + randomValue[random_y] > 500) {
            pos_y += randomValue[random_y];
        }
        if (pos_z + randomValue[random_z] > -8000 && pos_z + randomValue[random_z] < -3000) {
            pos_z += randomValue[random_z];
        }

        Node node{pos_x, pos_y, pos_z, 255, 0, 0, false, 20.0};
        route.push_back(node);
        counter++;
    }
}

/**
 * Here we create the path for challenge and call a collision check
 */
void Challenge::createPath() {
    for (unsigned int i = 0; i < route.size(); ++i) {
        checkCollision(i);
        renderNode(i);
    }
}

/**
 * Here we render our node
 */
void Challenge::renderNode(int index) {
    float tmp;

    if (!route.at(index).collision) {
        if (!toggle) {
            value += 0.01;
        } else value -= 0.01;

        if (value > 20.0) {
            value = 20.0;
        }

        if (value < 5.0) {
            value = 5.0;
        }

        tmp = value;
    } else tmp = route.at(index).motion;

    glPushMatrix();//center
    GLUquadricObj *quad_object = gluNewQuadric();
    glTranslated(route[index].x, route[index].y, route[index].z);
    glColor3ub(route[index].color_red, route[index].color_green, route[index].color_blue);
    gluSphere(quad_object, 10.0, 20, 20);
    glPopMatrix();

    glPushMatrix();//up
    glTranslated(route[index].x, route[index].y + tmp, route[index].z);
    glRotatef(-90, 1, 0, 0);
    glColor3ub(route[index].color_red, route[index].color_green, route[index].color_blue);
    glutSolidCone(5.0, 10, 20, 20);
    glPopMatrix();

    glPushMatrix();//down
    glTranslated(route[index].x, route[index].y - tmp, route[index].z);
    glRotatef(90, 1, 0, 0);
    glColor3ub(route[index].color_red, route[index].color_green, route[index].color_blue);
    glutSolidCone(5.0, 10, 20, 20);
    glPopMatrix();

    glPushMatrix();//left
    glTranslated(route[index].x - tmp, route[index].y, route[index].z);
    glRotatef(-90, 0, 1, 0);
    glColor3ub(route[index].color_red, route[index].color_green, route[index].color_blue);
    glutSolidCone(5.0, 10, 20, 20);
    glPopMatrix();

    glPushMatrix();//right
    glTranslated(route[index].x + tmp, route[index].y, route[index].z);
    glRotatef(90, 0, 1, 0);
    glColor3ub(route[index].color_red, route[index].color_green, route[index].color_blue);
    glutSolidCone(5.0, 10, 20, 20);
    glPopMatrix();

    glPushMatrix();//front
    glTranslated(route[index].x, route[index].y, route[index].z + tmp);
    glColor3ub(route[index].color_red, route[index].color_green, route[index].color_blue);
    glutSolidCone(5.0, 10, 20, 20);
    glPopMatrix();

    glPushMatrix();//back
    glTranslated(route[index].x, route[index].y, route[index].z - tmp);
    glColor3ub(route[index].color_red, route[index].color_green, route[index].color_blue);
    glRotatef(180, 0, 1, 0);
    glutSolidCone(5.0, 10, 20, 20);
    glPopMatrix();

    triggerNode();
}

/**
 * here we trigger node motion
 */
void Challenge::triggerNode() {
    if (value == 20.0) {
        toggle = true;
    }
    if (value == 5.0) {
        toggle = false;
    }
}

/**
 * Here we check if our plane touches node
 */
void Challenge::checkCollision(int index) {
    float value_x = route.at(index).x - Interface::planeMoveX;
    float value_y = route.at(index).y - Interface::planeMoveY;
    float value_z = route.at(index).z - Interface::planeMoveZ;

    //sensitivity - how much distance to node is allowed
    if (abs(value_x) < 15.0 && abs(value_y) < 15.0 && abs(value_z) < 15.0) {
        route[index].collision = true;
        route[index].color_red = 0;
        route[index].color_green = 255;
        route[index].color_blue = 0;
        route[index].motion = 20.0;
    }
}
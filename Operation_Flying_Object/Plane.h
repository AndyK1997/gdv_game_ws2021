#ifndef OPERATION_FLYING_OBJECT_PLANE_H
#define OPERATION_FLYING_OBJECT_PLANE_H

#include <GL/glut.h>
#include <iostream>
#include <array>

#include "imageloader.h"
#include "Interface.h"
#include "Environment.h"

using namespace std;

class Plane {
public:
    //main graph
    static void renderPlane();
    //texture
    static void loadTextures();
    // animations
    static void propellerRotation(bool rotation);
    //right wing rudder - right and left side
    static void aileronRudderRightAndLeft(bool right, bool left);
    //rudder back - side
    static void rudderBack();
    //rudder back - up and down
    static void elevatorRudderBack();
    //trigger landing gears
    static void LandingGearUpAndDown();
    //fly logic plane
    static void fly();
    //trigger speed
    static float speed();
    static void checkAndSetValues();

    static void moveUpDown(float circle);
    static void moveLeftRight(float circle);
    static void limitFieldPosition();
    static void moveForwardBackword(float circle);
    static float roleDegree ;
    static float pitchDegree;
    static float yawDegree;
    static void resetAngle();
    static bool toggle;
    static bool landingGearsReady;

    static int gear;
    static array <float,8> gears;
private:
    //texture
    static void pushTexture(const char *file);
    // rendered objects
    static void renderPlaneBody(float x, float y, float z);
    static void renderWing(float x, float y, float z);
    static void renderLandingGearLeft();
    static void renderLandingGearRight();
    static void renderPropellerBlade(float x, float y, float z);
    static void renderPropeller();
    static void renderCockpit(float x, float y, float z);
    static void renderPlaneTop(float x, float y, float z);
    static void renderPlaneBottom(float x, float y, float z);
    static void renderSphere(float radius);
    static void triangularPrism(float x, float y, float z);

    // graphs
    static void renderPosLightBack();
    static void renderWingLeft();
    static void renderWingRight();
    static void renderRear();
    static void renderBody();
    static void renderPosLightLeft();
    static void renderPosLightRight();
    static void renderLandingGearBack();

    static float _angle;
    static float _angle_L_GearsLeft;
    static float _angle_L_GearsRight;
    static float _angle_L_GearBackDoor;
    static float _angle_L_GearBackDownUp;
    static float _angle_L_GearBackRotation;
    static float driver_L_GearBackAndForth;
    static float aileron_left;
    static float aileron_right;
    static float rudder;
    static float elevator;
};

#endif //OPERATION_FLYING_OBJECT_PLANE_H

#ifndef OPERATION_FLYING_OBJECT_INTERFACE_H
#define OPERATION_FLYING_OBJECT_INTERFACE_H

#include "Plane.h"
#include "Environment.h"
class Interface {
public:
    //cam values
    static float cam_x;
    static float cam_y;
    static float cam_z;

    //plane angles
    static float camAnglePitch;
    static float camAngleYaw;

    //mouse values
    static float mouseX;
    static float mouseY;
    static bool Button1Down;

    static bool nightMode;
    static bool landingGearToggle;
    static bool speedToggle;
    static bool engineOnOff;
    static bool rudderLeft;
    static bool rudderRight;
    static bool elevatorUp;
    static bool elevatorDown;
    static bool aileronLeft;
    static bool aileronRight;
    static bool menu;

    static bool perspective;
    static bool back;
    static bool ego;

    static float planeMoveX;
    static float planeMoveY;
    static float planeMoveZ;

    static float position_x;
    static float position_y;
    static float position_z;

    static void handleKeyFunc(unsigned char key, int x, int y);
    static void handleSpecialFunc(int key, int x, int y );
    static void handleMouseButton(int button, int state, int x, int y);
    static void pressKey(unsigned char key);
    static void animate();

    static void viewOnBackCam();
private:
    static void egoCam();
    static void viewOnTop();
    static void viewOnTopFarAway();
    static void viewOnBottom();
    static void viewOnBottomFarAway();
    static void viewOnFront();
    static void viewOnLeft();
    static void viewOnRight();
    static void reset();
};


#endif //OPERATION_FLYING_OBJECT_INTERFACE_H

#include <iostream>
#include <cmath>
#include "Interface.h"

#define TRUE 1
#define FALSE 0
#define GL_PI 3.1415f

// cam values
float Interface::camAnglePitch = 0.0;
float Interface::camAngleYaw = 0.0;
// mouse values
bool  Interface::Button1Down = FALSE;
float Interface::mouseX = 0.0;
float Interface::mouseY = 0.0;

bool Interface::nightMode = false;
bool Interface::landingGearToggle = false;
bool Interface::speedToggle = false;
bool Interface::engineOnOff = true;

bool Interface::rudderLeft = false;
bool Interface::rudderRight = false;
bool Interface::elevatorUp = false;
bool Interface::elevatorDown = false;
bool Interface::aileronLeft = false;
bool Interface::aileronRight = false;

bool Interface::perspective = false;
bool Interface::ego = false;
bool Interface::back = false;
bool Interface::menu = false;

float Interface::planeMoveX = 0.0;
float Interface::planeMoveY = 1000.0;
float Interface::planeMoveZ = 0.0;

float Interface::cam_x = 0;
float Interface::cam_y = 0;
float Interface::cam_z = 0;
float Interface::position_x = 0.0;
float Interface::position_y = 0.0;
float Interface::position_z = 0.0;

/**
 * This handler proceeds normal keys
 */
void Interface::handleKeyFunc(unsigned char key, int x, int y) {
    cout << "key" <<(int)key<< endl;
    switch (key) {
        case 48: //press 0 - perspective mode
            perspective = !perspective;
            viewOnBackCam();
            break;
        case 49: //press 1 - view one back
            egoCam();
            break;
        case 50: //press 2 - view one back
            viewOnBackCam();
            break;
        case 51: //press 3 - view on front
            viewOnFront();
            break;
        case 52: //press 4 - view on the left side
            viewOnLeft();
            break;
        case 53: //press 5 - view on the right side
            viewOnRight();
            break;
        case 54: //press 6 - view on the top
            viewOnTop();
            break;
        case 55: //press 7 - view on the the top very far away
            viewOnTopFarAway();
            break;
        case 56: //press 8 - view on the bottom
            viewOnBottom();
            break;
        case 57: //press 9 - view on the bottom very far away
            viewOnBottomFarAway();
            break;
        case 97: //press a - left
            Plane::yawDegree++;
            rudderLeft = true;
            pressKey(key);
            break;
        case 100: //press d
            Plane::yawDegree--;
            rudderRight = true;
            pressKey(key);
            break;
        case 101: //press e
            Plane::roleDegree += 2;
            aileronRight = true;
            pressKey(key);
            break;
        case 113: //press q
            Plane::roleDegree -= 2;
            aileronLeft = true;
            pressKey(key);
            break;
        case 115: //press s - for down
            Plane::pitchDegree += 1;
            elevatorUp = false;
            elevatorDown = true;
            pressKey(key);
            break;
        case 119: //press w - for up
            Plane::pitchDegree -= 1;
            elevatorDown = false;
            elevatorUp = true;
            pressKey(key);
            break;
        case 27: //Escape key
            delete[] Environment::arrayMapTypes;
            Environment::arrayMapTypes = nullptr;
            delete[] Environment::cloudTypes;
            Environment::cloudTypes = nullptr;
            exit(0); //Exit the program
        case 120: //press x - landing gears
            if (landingGearToggle) {
                std::cout << "Landing gears on" << std::endl;
                landingGearToggle = false;
            } else {
                std::cout << "Landing gears off" << std::endl;
                landingGearToggle = true;
            }
            break;
        case 103: // press g - engine
            if (engineOnOff) {
                std::cout << "engine off" << std::endl;
                Plane::gear = 0;
                engineOnOff = false;
            } else {
                std::cout << "engine on" << std::endl;
                Plane::gear = 1;
                engineOnOff = true;
            }
            break;
        case 110: // press n - night mode
            if (!nightMode) {
                nightMode = true;
            } else {
                nightMode = false;
            }
            break;
        default:
            std::cerr << "Invalid key pressed" << std::endl;
    }
}

/**
 * This handler proceeds special keys
 */
void Interface::handleSpecialFunc(int key, int x, int y) {
    switch (key) {
        case GLUT_KEY_LEFT:
            //do something
            break;
        case GLUT_KEY_RIGHT:
            //do something
            break;
        case GLUT_KEY_UP:
            if (Plane::gear < Plane::gears.size() - 1) {
                Plane::gear++;
            }
            break;
        case GLUT_KEY_DOWN:
            if (Plane::gear > 0) {
                Plane::gear--;
            }
            break;
    }
}

/**
 * Here we animate all movable parts on the plane
 */
void Interface::animate() {
    //ToDo: try to turn off engine slowly
    Plane::propellerRotation(engineOnOff);
    Plane::aileronRudderRightAndLeft(aileronRight, aileronLeft);
    Plane::elevatorRudderBack();
    Plane::rudderBack();
    Plane::LandingGearUpAndDown();
}

/***
 * This handler let us zoom by mouse click
 */
void Interface::handleMouseButton(int button, int state, int x, int y) {
    if (button == GLUT_LEFT_BUTTON && !menu) {// left button pressed
        Button1Down = (state == GLUT_DOWN) ? TRUE : FALSE;
        if (cam_z >= -100) {
            perspective = false;
            cam_z -= 10;
            planeMoveZ -= cam_z;
        }
        position_x = 0;
        position_y = 0;
        position_z = 0;

    } else if (button == GLUT_RIGHT_BUTTON && !menu) {// right button pressed
        Button1Down = (state == GLUT_DOWN) ? TRUE : FALSE;
        if (cam_z <= 100) {
            perspective = false;
            cam_z += 10;
            planeMoveZ += cam_z;
        }
        position_x = 0;
        position_y = 0;
        position_z = 0;
    }
}

/**
 * Here we change camera view
 */
void Interface::pressKey(unsigned char key) {
    float y_rot, x_rot;
    float _factor, _normal = 0.8, _ego = 0.16;

    if (ego) {
        _factor = _ego;
    } else _factor = _normal;

    if (perspective) {//perspective must have special factor
        //calculate
        y_rot = (camAngleYaw / 180 * GL_PI);
        x_rot = (camAnglePitch / 180 * GL_PI);

        if (key == 119) {//w - up
            position_x -= sin(y_rot) * _factor;
            position_y += cos(y_rot) * _factor;
            position_z += sin(x_rot) * _factor;
            if (back && perspective && !ego) {
                if (camAnglePitch >= 0.0) {
                    cam_y -= 1;
                } else if (camAnglePitch == 60.0) {
                    viewOnBackCam();
                }
            }
        } else if (key == 115) {//s - down
            position_x += sin(y_rot) * _factor;
            position_y -= cos(y_rot) * _factor;
            position_z -= sin(x_rot) * _factor;
            if (back && perspective && !ego) {
                if (camAnglePitch >= 0.0) {
                    cam_y += 1;
                } else if (camAnglePitch == 300.0) {
                    viewOnBackCam();
                }
            }
        } else if (key == 100) {//d - right
            position_x += cos(y_rot) * _factor;
            position_z += sin(y_rot) * _factor;
            if (back && perspective && !ego) {
                if (camAngleYaw >= 0) {
                    cam_x -= 1;
                    if (camAngleYaw == 270.0) {

                    }
                }
            }
        } else if (key == 97) {//a - left
            position_x -= cos(y_rot) * _factor;
            position_z -= sin(y_rot) * _factor;
            if (back && perspective && !ego) {
                if (camAngleYaw >= 0) {
                    cam_x += 1;
                    if (camAngleYaw == 270.0) {//on rigth
                        cam_x = 100;
                        cam_y = 5;
                        cam_z = -40;
                        position_x = 0;
                        position_y = 0;
                        position_z = 0;
                    }
                    if (camAngleYaw == 180.0) {// on front
                        cam_x = 0;
                        cam_y = 5;
                        cam_z = -80;
                        position_x = 0;
                        position_y = 0;
                        position_z = 0;
                    }
                    if(camAngleYaw == 90.0) {
                        cam_x = -100;
                        cam_y = 5;
                        cam_z = -40;
                        position_x = 0;
                        position_y = 0;
                        position_z = 0;
                    }
                    if(camAngleYaw == 60.0){
                        cam_x = 0;
                        cam_y = 8;
                        cam_z = 40;
                        position_x = 0;
                        position_y = 0;
                        position_z = 0;
                    }
                    if(camAngleYaw == 0.0) {
                        cam_x = 0;
                        cam_y = 8;
                        cam_z = 40;
                        position_x = 0;
                        position_y = 0;
                        position_z = 0;
                    }
                }
            }
        }
    }
}

void Interface::egoCam() {
    position_x = 0;
    position_y = 0;
    position_z = -10;
    cam_y = 2.0;
    cam_z = 0.3;
    ego = true;
    perspective = true;

}

void Interface::viewOnBackCam() {
    cam_x = 0;
    cam_y = 8;
    cam_z = 40;
    back = true;
    ego = false;
    position_x = 0;
    position_y = 0;
    position_z = 0;
}

void Interface::viewOnTop() {
    cam_x = 1;
    cam_y = 100;
    cam_z = -40;
    reset();
}

void Interface::viewOnBottom() {
    cam_x = 1;
    cam_y = -100;
    cam_z = -40;
    reset();
}

void Interface::viewOnFront() {
    cam_x = 0;
    cam_y = 5;
    cam_z = -80;
    reset();
}

void Interface::viewOnLeft() {
    cam_x = -100;
    cam_y = 5;
    cam_z = -40;
    reset();
}

void Interface::viewOnRight() {
    cam_x = 100;
    cam_y = 5;
    cam_z = -40;
    reset();
}

void Interface::viewOnTopFarAway() {
    cam_x = 1;
    cam_y = 200;
    cam_z = -40;
    reset();
}

void Interface::viewOnBottomFarAway() {
    cam_x = 1;
    cam_y = -200;
    cam_z = -40;
    reset();
}

void Interface::reset() {
    ego = false;
    perspective = false;
    back = false;
    position_x = 0;
    position_y = 0;
    position_z = 0;
}









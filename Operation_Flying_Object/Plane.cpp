#include "Plane.h"

float Plane::_angle = 0.0f;
float Plane::_angle_L_GearsLeft = 0.0f;
float Plane::_angle_L_GearsRight = 0.0f;
float Plane::_angle_L_GearBackDoor = 120.0f;
float Plane::_angle_L_GearBackDownUp = 0.0f;
float Plane::_angle_L_GearBackRotation = 0.0f;
float Plane::driver_L_GearBackAndForth = -9.5f;
float Plane::aileron_right = 0.0f;
float Plane::aileron_left = 0.0f;
float Plane::rudder = 0.0f;
float Plane::elevator = 0.0f;
float Plane::roleDegree = 0.0f;
float Plane::pitchDegree = 0.0f;
float Plane::yawDegree = 0.0f;
std::vector<GLuint> planeTexture;

bool Plane::toggle = false;

int Plane::gear = 2;
array<float, 8> Plane::gears = {0.0f, 0.4f, 1.0f, 1.5f, 2.0f, 2.5f, 3.0f, 3.5f};

/**
 * renders part of graph - plane body
 */
void Plane::renderPlaneBody(float x, float y, float z) {

    const GLfloat bodyThickness = 2.0f;
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, planeTexture[0]);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    //front
    glColor3ub(196, 196, 196);
    glBegin(GL_POLYGON);
    glNormal3f(0.0f, 0.0f, -1.0f);
    glTexCoord2f(0.0f, 0.0f);
    glVertex3f(-x, y, z);
    glTexCoord2f(1.0f, 0.0f);
    glVertex3f(-x, -y, z);
    glTexCoord2f(1.0f, 1.0f);
    glVertex3f(x - 1.0f, -y, z);
    glTexCoord2f(0.0f, 1.0f);
    glVertex3f(x, y - 0.5f, z);
    glVertex3f(x, y, z);
    glEnd();

    //right
    glColor3ub(215, 0, 0);
    glBegin(GL_POLYGON);
    glNormal3f(1.0f, 0.0f, 0.0f);
    glTexCoord2f(0.0f, 0.0f);
    glVertex3f(x, y, z + bodyThickness);
    glTexCoord2f(1.0f, 0.0f);
    glVertex3f(x, y - 0.5f, z + bodyThickness);
    glTexCoord2f(1.0f, 1.0f);
    glVertex3f(x, y - 0.5f, z);
    glTexCoord2f(0.0f, 1.0f);
    glVertex3f(x, y, z);
    glEnd();

    //glColor3ub(196,196,196);
    glBegin(GL_POLYGON);
    glNormal3f(1.0f, 0.0f, 0.0f);
    glTexCoord2f(0.0f, 0.0f);
    glVertex3f(x, y - 0.5f, z + bodyThickness);
    glTexCoord2f(1.0f, 0.0f);
    glVertex3f(x - 1.0f, -y, z + bodyThickness);
    glTexCoord2f(1.0f, 1.0f);
    glVertex3f(x - 1.0f, -y, z);
    glTexCoord2f(0.0f, 1.0f);
    glVertex3f(x, y - 0.5f, z);
    glEnd();

    //back
    glColor3ub(196, 196, 196);
    glBegin(GL_POLYGON);
    glNormal3f(0.0f, 0.0f, 1.0f);
    glTexCoord2f(0.0f, 0.0f);
    glVertex3f(-x, y, z + bodyThickness);
    glTexCoord2f(1.0f, 0.0f);
    glVertex3f(-x, -y, z + bodyThickness);
    glTexCoord2f(1.0f, 1.0f);
    glVertex3f(x - 1.0f, -y, z + bodyThickness);
    glTexCoord2f(0.0f, 1.0f);
    glVertex3f(x, y - 0.5, z + bodyThickness);
    glVertex3f(x, y, z + bodyThickness);
    glEnd();

    //left
    glColor3ub(215, 0, 0);
    glBegin(GL_POLYGON);
    glNormal3f(-1.0f, 0.0f, 0.0f);
    glVertex3f(-x, y, z + bodyThickness);
    glVertex3f(-x, -y, z + bodyThickness);
    glVertex3f(-x, -y, z);
    glVertex3f(-x, y, z);
    glEnd();

    //top
    glColor3ub(196, 196, 196);
    glBegin(GL_POLYGON);
    glNormal3f(0.0f, 1.0f, 0.0f);
    glTexCoord2f(0.0f, 0.0f);
    glVertex3f(-x, y, z + bodyThickness);
    glTexCoord2f(1.0f, 0.0f);
    glVertex3f(-x, y, z);
    glTexCoord2f(1.0f, 1.0f);
    glVertex3f(x, y, z);
    glTexCoord2f(0.0f, 1.0f);
    glVertex3f(x, y, z + bodyThickness);
    glEnd();

    //bottom;
    glColor3ub(196, 196, 196);
    glBegin(GL_POLYGON);
    glNormal3f(0.0f, -1.0f, 0.0f);
    glVertex3f(-x, -y, z + bodyThickness);
    glVertex3f(-x, -y, z);
    glVertex3f(x - 1.0f, -y, z);
    glVertex3f(x - 1.0f, -y, z + bodyThickness);
    glEnd();
    glDisable(GL_TEXTURE_2D);
}

/**
 * renders part of graph - wing
 */
void Plane::renderWing(float x, float y, float z) {
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, planeTexture[0]);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    // front
    glBegin(GL_POLYGON);
    glNormal3f(0.0f, 0.0f, 1.0f);
    glTexCoord2f(0.0f, 0.0f);
    glVertex3f(-x / 2.0f, -y / 2.0f, +z / 2.0f);
    glTexCoord2f(1.0f, 0.0f);
    glVertex3f(+x / 2.0f, -y / 2.0f, +z / 2.0f);
    glTexCoord2f(1.0f, 1.0f);
    glVertex3f(+x / 2.0f, +y / 2.0f, +z / 2.0f);
    glTexCoord2f(0.0f, 1.0f);
    glVertex3f(-x / 2.0f, +y / 2.0f, +z / 2.0f);
    glEnd();

    // right
    glBegin(GL_POLYGON);
    glNormal3f(1.0f, 0.0f, 0.0f);
    glTexCoord2f(0.0f, 0.0f);
    glVertex3f(+x / 2.0f, -y / 2.0f, +z / 2.0f);
    glTexCoord2f(1.0f, 0.0f);
    glVertex3f(+x / 2.0f, -y / 2.0f, -z / 2.0f);
    glTexCoord2f(1.0f, 1.0f);
    glVertex3f(+x / 2.0f, +y / 2.0f, -z / 2.0f);
    glTexCoord2f(0.0f, 1.0f);
    glVertex3f(+x / 2.0f, +y / 2.0f, +z / 2.0f);
    glEnd();

    // back
    glBegin(GL_POLYGON);
    glNormal3f(0.0f, 0.0f, -1.0f);
    glTexCoord2f(0.0f, 0.0f);
    glVertex3f(+x / 2.0f, +y / 2.0f, -z / 2.0f);
    glTexCoord2f(1.0f, 0.0f);
    glVertex3f(+x / 2.0f, -y / 2.0f, -z / 2.0f);
    glTexCoord2f(1.0f, 1.0f);
    glVertex3f(-x / 2.0f, -y / 2.0f, -z / 2.0f);
    glTexCoord2f(0.0f, 1.0f);
    glVertex3f(-x / 2.0f, +y / 2.0f, -z / 2.0f);
    glEnd();

    // left
    glBegin(GL_POLYGON);
    glNormal3f(-1.0f, 0.0f, 0.0f);
    glTexCoord2f(0.0f, 0.0f);
    glVertex3f(-x / 2.0f, +y / 2.0f, -z / 2.0f);
    glTexCoord2f(1.0f, 0.0f);
    glVertex3f(-x / 2.0f, -y / 2.0f, -z / 2.0f);
    glTexCoord2f(1.0f, 1.0f);
    glVertex3f(-x / 2.0f, -y / 2.0f, +z / 2.0f);
    glTexCoord2f(0.0f, 1.0f);
    glVertex3f(-x / 2.0f, +y / 2.0f, +z / 2.0f);
    glEnd();

    // top
    glBegin(GL_POLYGON);
    glNormal3f(0.0f, 1.0f, 0.0f);
    glTexCoord2f(0.0f, 0.0f);
    glVertex3f(-x / 2.0f, +y / 2.0f, +z / 2.0f);
    glTexCoord2f(1.0f, 0.0f);
    glVertex3f(+x / 2.0f, +y / 2.0f, +z / 2.0f);
    glTexCoord2f(1.0f, 1.0f);
    glVertex3f(+x / 2.0f, +y / 2.0f, -z / 2.0f);
    glTexCoord2f(0.0f, 1.0f);
    glVertex3f(-x / 2.0f, +y / 2.0f, -z / 2.0f);
    glEnd();

    // top
    glBegin(GL_POLYGON);
    glNormal3f(0.0f, -1.0f, 0.0f);
    glTexCoord2f(0.0f, 0.0f);
    glVertex3f(-x / 2.0f, -y / 2.0f, -z / 2.0f);
    glTexCoord2f(1.0f, 0.0f);
    glVertex3f(+x / 2.0f, -y / 2.0f, -z / 2.0f);
    glTexCoord2f(1.0f, 1.0f);
    glVertex3f(+x / 2.0f, -y / 2.0f, +z / 2.0f);
    glTexCoord2f(0.0f, 1.0f);
    glVertex3f(-x / 2.0f, -y / 2.0f, +z / 2.0f);
    glEnd();
    glDisable(GL_TEXTURE_2D);
}

/**
 * renders part of graph - propeller blade
 */
void Plane::renderPropellerBlade(float x, float y, float z) {
    // front
    glColor3f(0.0f, 0.0f, 0.0f);
    glBegin(GL_POLYGON);
    glNormal3f(0.0f, 0.0f, 1.0f);
    glVertex3f(-x / 2.0f, -y / 2.0f, +z / 2.0f);
    glVertex3f(+x / 2.0f, -y / 2.0f, +z / 2.0f);
    glVertex3f(+x / 2.0f, +y / 2.0f, +z / 2.0f);
    glVertex3f(-x / 2.0f, +y / 2.0f, +z / 2.0f);
    glEnd();

    // right
    glColor3f(1.0f, 0.0f, 0.0f);
    glBegin(GL_POLYGON);
    glNormal3f(1.0f, 0.0f, 0.0f);
    glVertex3f(+x / 2.0f, -y / 2.0f, +z / 2.0f);
    glVertex3f(+x / 2.0f, -y / 2.0f, -z / 2.0f);
    glVertex3f(+x / 2.0f, +y / 2.0f, -z / 2.0f);
    glVertex3f(+x / 2.0f, +y / 2.0f, +z / 2.0f);
    glEnd();

    // back
    glColor3f(0.0f, 0.0f, 0.0f);
    glBegin(GL_POLYGON);
    glNormal3f(0.0f, 0.0f, -1.0f);
    glVertex3f(+x / 2.0f, +y / 2.0f, -z / 2.0f);
    glVertex3f(+x / 2.0f, -y / 2.0f, -z / 2.0f);
    glVertex3f(-x / 2.0f, -y / 2.0f, -z / 2.0f);
    glVertex3f(-x / 2.0f, +y / 2.0f, -z / 2.0f);
    glEnd();

    // left
    glColor3f(1.0f, 0.0f, 0.0f);
    glBegin(GL_POLYGON);
    glNormal3f(-1.0f, 0.0f, 0.0f);
    glVertex3f(-x / 2.0f, +y / 2.0f, -z / 2.0f);
    glVertex3f(-x / 2.0f, -y / 2.0f, -z / 2.0f);
    glVertex3f(-x / 2.0f, -y / 2.0f, +z / 2.0f);
    glVertex3f(-x / 2.0f, +y / 2.0f, +z / 2.0f);
    glEnd();

    // top
    glColor3f(0.0f, 0.0f, 0.0f);
    glBegin(GL_POLYGON);
    glNormal3f(0.0f, 1.0f, 0.0f);
    glVertex3f(-x / 2.0f, +y / 2.0f, +z / 2.0f);
    glVertex3f(+x / 2.0f, +y / 2.0f, +z / 2.0f);
    glVertex3f(+x / 2.0f, +y / 2.0f, -z / 2.0f);
    glVertex3f(-x / 2.0f, +y / 2.0f, -z / 2.0f);
    glEnd();

    // bottom
    glColor3f(0.0f, 0.0f, 0.0f);
    glBegin(GL_POLYGON);
    glNormal3f(0.0f, -1.0f, 0.0f);
    glVertex3f(-x / 2.0f, -y / 2.0f, -z / 2.0f);
    glVertex3f(+x / 2.0f, -y / 2.0f, -z / 2.0f);
    glVertex3f(+x / 2.0f, -y / 2.0f, +z / 2.0f);
    glVertex3f(-x / 2.0f, -y / 2.0f, +z / 2.0f);
    glEnd();
}

/**
 * renders part of graph - cockpit
 */
void Plane::renderCockpit(float x, float y, float z) {
    const float THICKNESS = 1.5;
    const float OFFSET = 1.0;
    glColor3f(0.0f, 0.90f, 1.0f);

    // front
    glBegin(GL_POLYGON);
    glNormal3f(0.0f, 0.0f, -1.0f);
    glVertex3f(-x + OFFSET, y, z);
    glVertex3f(-x, -y, z);
    glVertex3f(x, -y, z);
    glVertex3f(x - OFFSET, y, z);
    glEnd();


    // right
    glBegin(GL_POLYGON);
    glNormal3f(1.0f, 0.0f, 0.0f);
    glVertex3f(x - OFFSET, y, z + THICKNESS);
    glVertex3f(x, -y, z + THICKNESS);
    glVertex3f(x, -y, z);
    glVertex3f(x - OFFSET, y, z);
    glEnd();

    // back
    glBegin(GL_POLYGON);
    glNormal3f(0.0f, 0.0f, 1.0f);
    glVertex3f(-x + OFFSET, y, z + THICKNESS);
    glVertex3f(-x, -y, z + THICKNESS);
    glVertex3f(x, -y, z + THICKNESS);
    glVertex3f(x - OFFSET, y, z + THICKNESS);
    glEnd();

    // left
    glBegin(GL_POLYGON);
    glNormal3f(-1.0f, 0.0f, 0.0f);
    glVertex3f(-x + OFFSET, y, z + THICKNESS);
    glVertex3f(-x, -y, z + THICKNESS);
    glVertex3f(-x, -y, z);
    glVertex3f(-x + OFFSET, y, z);
    glEnd();

    // top
    glBegin(GL_POLYGON);
    glNormal3f(0.0f, 1.0f, 0.0f);
    glVertex3f(-x + OFFSET, y, z + THICKNESS);
    glVertex3f(-x + OFFSET, y, z);
    glVertex3f(x - OFFSET, y, z);
    glVertex3f(x - OFFSET, y, z + THICKNESS);
    glEnd();
}

/**
 * renders part of graph - plane top
 */
void Plane::renderPlaneTop(float x, float y, float z) {
    const float THICKNESS = 2.0;
    const float OFFSET = 7.5;
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, planeTexture[0]);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glColor3ub(14, 0, 215);

    //front - top in space
    glBegin(GL_POLYGON);
    glNormal3f(0.0f, 1.0f, 0.0f);
    glTexCoord2f(0.0f, 0.0f);
    glVertex3f(-x, y, z);
    glTexCoord2f(1.0f, 0.0f);
    glVertex3f(-x, -y, z);
    glTexCoord2f(1.0f, 1.0f);
    glVertex3f(x, -y, z);
    glTexCoord2f(0.0f, 1.0f);
    glVertex3f(x - OFFSET, y, z);
    glEnd();

    //right
    glColor3ub(196, 196, 196);
    glBegin(GL_POLYGON);
    glTexCoord2f(0.0f, 0.0f);
    glVertex3f(x - OFFSET, y, z + THICKNESS);
    glTexCoord2f(1.0f, 0.0f);
    glVertex3f(x, -y, z + THICKNESS);
    glTexCoord2f(1.0f, 1.0f);
    glVertex3f(x, -y, z);
    glTexCoord2f(0.0f, 1.0f);
    glVertex3f(x - OFFSET, y, z);
    glEnd();

    //left - front in space
    glColor3ub(215, 0, 0);
    glBegin(GL_POLYGON);
    glNormal3f(0.0f, 0.0f, 1.0f);
    glTexCoord2f(0.0f, 0.0f);
    glVertex3f(-x, y, z + THICKNESS);
    glTexCoord2f(1.0f, 0.0f);
    glVertex3f(-x, -y, z + THICKNESS);
    glTexCoord2f(1.0f, 1.0f);
    glVertex3f(-x, -y, z);
    glTexCoord2f(0.0f, 1.0f);
    glVertex3f(-x, y, z);
    glEnd();

    //back
    glColor3ub(14, 0, 215);
    glBegin(GL_POLYGON);
    glTexCoord2f(0.0f, 0.0f);
    glVertex3f(-x, y, z + THICKNESS);
    glTexCoord2f(1.0f, 0.0f);
    glVertex3f(-x, -y, z + THICKNESS);
    glTexCoord2f(1.0f, 1.0f);
    glVertex3f(x, -y, z + THICKNESS);
    glTexCoord2f(0.0f, 1.0f);
    glVertex3f(x - OFFSET, y, z + THICKNESS);
    glEnd();

    //top - top in space
    glColor3ub(196, 196, 196);
    glBegin(GL_POLYGON);
    glNormal3f(0.0f, 1.0f, 0.0f);
    glTexCoord2f(0.0f, 0.0f);
    glVertex3f(-x, y, z + THICKNESS);
    glTexCoord2f(1.0f, 0.0f);
    glVertex3f(-x, y, z);
    glTexCoord2f(1.0f, 1.0f);
    glVertex3f(x - OFFSET, y, z);
    glTexCoord2f(0.0f, 1.0f);
    glVertex3f(x - OFFSET, y, z + THICKNESS);
    glEnd();
    glDisable(GL_TEXTURE_2D);
}

/**
 * renders part of graph - plane bottom
 */
void Plane::renderPlaneBottom(float x, float y, float z) {
    const float THICKNESS = 2.0;
    const float OFFSET = 1.0;
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, planeTexture[0]);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    //front - back in space
    glColor3ub(196, 196, 196);
    glBegin(GL_POLYGON);
    glNormal3f(0.0f, 0.0f, -1.0f);
    glTexCoord2f(0.0f, 0.0f);
    glVertex3f(-x, y, z);
    glTexCoord2f(1.0f, 0.0f);
    glVertex3f(-x + OFFSET, -y, z);
    glTexCoord2f(1.0f, 1.0f);
    glVertex3f(x - OFFSET, -y, z);
    glTexCoord2f(0.0f, 1.0f);
    glVertex3f(x, y, z);
    glEnd();

    //right - bottom back in space
    glColor3ub(196, 196, 196);
    glBegin(GL_POLYGON);
    glNormal3f(0.0f, -1.0f, 0.0f);
    glTexCoord2f(0.0f, 0.0f);
    glVertex3f(x - 1.5, y, z + THICKNESS);
    glTexCoord2f(1.0f, 0.0f);
    glVertex3f(x - 1.5, -y, z + THICKNESS);
    glTexCoord2f(1.0f, 1.0f);
    glVertex3f(x - 1.5, -y, z);
    glTexCoord2f(0.0f, 1.0f);
    glVertex3f(x - 1.5, y, z);
    glEnd();

    //left - bottom in space
    glColor3ub(215, 0, 0);
    glBegin(GL_POLYGON);
    glNormal3f(0.0f, -1.0f, 0.0f);
    glTexCoord2f(0.0f, 0.0f);
    glVertex3f(-x, y, z + THICKNESS);
    glTexCoord2f(1.0f, 0.0f);
    glVertex3f(-x + OFFSET, -y, z + THICKNESS);
    glTexCoord2f(1.0f, 1.0f);
    glVertex3f(-x + OFFSET, -y, z);
    glTexCoord2f(0.0f, 1.0f);
    glVertex3f(-x, y, z);
    glEnd();

    //back - left in space
    glColor3ub(196, 196, 196);
    glBegin(GL_POLYGON);
    glNormal3f(0.0f, 0.0f, 1.0f);
    glTexCoord2f(0.0f, 0.0f);
    glVertex3f(-x, y, z + THICKNESS);
    glTexCoord2f(1.0f, 0.0f);
    glVertex3f(-x + OFFSET, -y, z + THICKNESS);
    glTexCoord2f(1.0f, 1.0f);
    glVertex3f(x - OFFSET, -y, z + THICKNESS);
    glTexCoord2f(0.0f, 1.0f);
    glVertex3f(x, y, z + THICKNESS);
    glEnd();

    //bottom - bottom in space
    glColor3ub(196, 196, 196);
    glBegin(GL_POLYGON);
    glNormal3f(0.0f, -1.0f, 0.0f);
    glTexCoord2f(0.0f, 0.0f);
    glVertex3f(-x + OFFSET, -y, z + THICKNESS);
    glTexCoord2f(1.0f, 0.0f);
    glVertex3f(-x + OFFSET, -y, z);
    glTexCoord2f(1.0f, 1.0f);
    glVertex3f(x - OFFSET, -y, z);
    glTexCoord2f(0.0f, 1.0f);
    glVertex3f(x - OFFSET, -y, z + THICKNESS);
    glEnd();
    glDisable(GL_TEXTURE_2D);
}

/**
 * here check values for our fly logic
 */
void Plane::checkAndSetValues() {
    Interface::rudderLeft = false;
    Interface::rudderRight = false;
    Interface::aileronLeft = false;
    Interface::aileronRight = false;
}

/**
 * triggers rotation of propeller in graph
 */
void Plane::propellerRotation(bool rotation) {
    GLfloat speed = 5 + 10 * gears.at(gear);
    if (rotation) {
        Plane::_angle += speed;
        if (Plane::_angle == 360) {
            Plane::_angle = 0;
        }
    }
}

/**
 * triggers rudder in graph
 */
void Plane::rudderBack() {
    if (yawDegree < 0.0f) { // press A: right up and left down
        Plane::rudder += 1.0f;
        if (Plane::rudder > 30.0f) {
            Plane::rudder = 30.0f;
        }
    } else if (yawDegree > 0.0f) { // press D: left up and right down
        Plane::rudder -= 1.0f;
        if (Plane::rudder < -30.0f) {
            Plane::rudder = -30.0f;
        }
    } else if (yawDegree == 0.0f) {
        rudder = 0.0f;
    }
}

/**
 * triggers elevator in graph
 */
void Plane::elevatorRudderBack() {
    if (pitchDegree < 0.0f) { // press E: right up and left down
        Plane::elevator += 1.0f;
        if (Plane::elevator > 30.0f) {
            Plane::elevator = 30.0f;
        }
    } else if (pitchDegree > 0.0f) { // press Q: left up and right down
        Plane::elevator -= 1.0f;
        if (Plane::elevator < -30.0f) {
            Plane::elevator = -30.0f;
        }
    } else if (pitchDegree == 0.0f || pitchDegree == -0.0f) {
        Plane::elevator = 0.0f;
    }
    //std::cout<<"Pitch Degree: "<<Interface::pitchDegree<<std::endl;
}

/**
 * triggers aileron rudder in graph
 */
//ToDo: Check right values for direction
void Plane::aileronRudderRightAndLeft(bool right, bool left) {
    if (!right && left) { // press E: right up and left down
        Plane::aileron_right += 1.0f;
        Plane::aileron_left -= 1.0f;
        if (Plane::aileron_right >= 30.0f) {
            Plane::aileron_right = 30.0f;
        } else if (Plane::aileron_left <= -30.0f) {
            Plane::aileron_left = -30.0f;
        }
    } else if (right && !left) { // press Q: left up and right down
        Plane::aileron_left += 1.0f;
        Plane::aileron_right -= 1.0f;
        if (Plane::aileron_left >= 30.0f) {
            Plane::aileron_left = 30.0f;
        } else if (Plane::aileron_right <= -30.0f) {
            Plane::aileron_right = -30.0f;
        }
    }

    if (roleDegree == 0.0f) {
        Plane::aileron_right = 0.0f;
        Plane::aileron_left = 0.0f;
    }

    if (Interface::landingGearToggle) {
        aileron_right = -30.0;
        aileron_left = -30.0;
    }

}

/**
 * builds a graph to animate specific nodes
 */
void Plane::renderPlane() {
    //new graph
    glPushMatrix();
    //body node
    renderBody();
    //landing gears back
    renderLandingGearBack();
    //wing right node
    renderWingRight();
    //landing gears right
    renderLandingGearRight();
    //wing left node
    renderWingLeft();
    //landing gears left
    renderLandingGearLeft();
    //tail node
    renderRear();
    //propeller node
    renderPropeller();
    glPopMatrix();
}

/**
 * renders part of graph - landing gear back
 */
void Plane::renderLandingGearBack() {
    glPushMatrix();//fix point door - not visible
    glTranslatef(0.0f, -1.58f, -11.0f);
    renderSphere(0.0f);

    glPushMatrix();
    glRotatef(_angle_L_GearBackDoor, 1, 0, 0);
    glTranslatef(0.0f, -0.55f, 0.0f);
    renderWing(2.0f, 1.2f, 0.02f);
    glPopMatrix();
    glPopMatrix();


    glPushMatrix();//fix point landing gears - not visible
    glTranslatef(0.0f, -1.4f, driver_L_GearBackAndForth);
    glRotatef(90, 0, 1, 0);
    glRotatef(180, 1, 0, 0);
    glRotatef(_angle_L_GearBackRotation, 1, 0, 0);
    renderSphere(0.0f);

    glPushMatrix();//first arm
    glTranslatef(-1.0f, 0.0f, 0.0f);
    Plane::renderPropellerBlade(2.0f, 0.1f, 0.1f);
    glPopMatrix();

    glPushMatrix();//rotation point
    glTranslatef(0.1f, 0.0f, 0.0f);
    glColor3ub(30, 30, 30);
    renderSphere(0.1f);

    glPushMatrix();//second arm
    glRotatef(_angle_L_GearBackDownUp, 0.0f, 1.0f, 0.0f);
    glTranslatef(0.8f, 0.0f, 0.0f);
    Plane::renderPropellerBlade(1.5f, 0.1f, 0.1f);

    //wheel
    glRotatef(90, 1, 0, 0);
    glTranslatef(0.6f, 0.0f, 0.15f);
    glColor3ub(30, 30, 30);
    glutSolidTorus(0.15f, 0.2f, 20, 20);
    glPopMatrix();

}

/**
 * renders part of graph - landing gear left
 */
void Plane::renderLandingGearLeft() {
    //sphere landing gears
    glPushMatrix();
    glTranslatef(-5.9f, -0.25f, -3.8f);
    glColor3ub(215, 0, 0);
    renderSphere(0.25f);

    //landing gears - first rotate object and than move to position
    glPushMatrix();
    glRotatef(_angle_L_GearsLeft, 0, 0, 1);
    glTranslatef(2, 0, 0);
    Plane::renderPropellerBlade(3.5f, 0.2f, 0.3f);

    //wheel
    glRotatef(90, 1, 0, 0);
    glTranslatef(1.7f, 0.0f, 0.45f);
    glColor3ub(30, 30, 30);
    glEnable(GL_NORMALIZE);
    glutSolidTorus(0.35, 0.35, 20, 20);
    glPopMatrix();
}

/**
 * renders part of graph - landing gear right
 */
void Plane::renderLandingGearRight() {
    //sphere landing gears
    glPushMatrix();
    glTranslatef(5.9f, -0.25f, -3.8f);
    glColor3ub(215, 0, 0);
    renderSphere(0.25);

    //landing gears - first rotate object and than move to position
    glPushMatrix();
    glRotatef(_angle_L_GearsRight, 0, 0, 1);
    glTranslatef(-2, 0, 0);
    Plane::renderPropellerBlade(3.5f, 0.2f, 0.35f);

    //wheel
    glRotatef(90, 1, 0, 0);
    glTranslatef(-1.7f, 0.0f, 0.45f);
    glColor3ub(30, 30, 30);
    glEnable(GL_NORMALIZE);
    glutSolidTorus(0.35, 0.35, 20, 20);
    glPopMatrix();
}

/**
 * renders part of graph - wing right
 */
void Plane::renderWingRight() {
    //new Graph
    glPushMatrix();
    //wing right node
    glPushMatrix();
    glRotatef(90, 1, 0, 0);
    glTranslatef(-5.0f, -3.5f, 0.0f);
    glColor3ub(14, 0, 215);
    Plane::renderWing(10.0f, 2.0f, 0.2f);
    glPushMatrix();
    glPopMatrix();
    //front prism right wing
    glPushMatrix();
    glTranslatef(0.0, 0.0f, -1.5);
    glRotatef(90, 0, 0, 1);
    glColor3ub(14, 0, 215);
    triangularPrism(0.1, 10.0, 1.0);
    glPushMatrix();
    glPopMatrix();
    //prism end
    glPushMatrix();
    glTranslatef(10.0, 0.0f, -3.8);
    glRotatef(90, 0, 0, 1);
    glColor3ub(14, 0, 215);
    triangularPrism(0.1, -1.0, 1.3);
    glPushMatrix();
    glPopMatrix();
    // position light right
    glPushMatrix();
    glTranslatef(-11.0f, 0.0f, -5.0f);
    Plane::renderPosLightRight();
    glPushMatrix();
    glPopMatrix();
    //aileron rudder right node
    glPushMatrix();
    glRotatef(90, 1, 0, 0);
    glTranslatef(-5.5f, -4.8f, 0.0f);
    glRotatef(aileron_right, 1, 0, 0);
    glColor3ub(215, 0, 0);
    Plane::renderWing(9.0f, 0.5f, 0.1f);
    glPushMatrix();
    glPopMatrix();
    //prism landing gears
    glPushMatrix();
    glTranslatef(6.0, -0.35f, -3.8);
    glRotatef(90, 0, 1, 0);
    glRotatef(90, 1, 0, 0);
    glColor3ub(215, 0, 0);
    triangularPrism(0.3, 0.3, 0.3);
    glPushMatrix();
    glPopMatrix();
}

/**
 * renders part of graph - wing left
 */
void Plane::renderWingLeft() {
    // new Graph
    glPushMatrix();
    // wing left node
    glPushMatrix();
    glRotatef(90, 1, 0, 0);
    glTranslatef(5.0f, -3.5f, 0.0f);
    glColor3ub(14, 0, 215);
    Plane::renderWing(10.0f, 2.0f, 0.2f);
    glPushMatrix();
    glPopMatrix();
    //front prism right wing
    glPushMatrix();
    glTranslatef(0.0, 0.0f, -1.5);
    glRotatef(-90, 0, 0, 1);
    glColor3ub(14, 0, 215);
    triangularPrism(0.1, 10.0, 1.0);
    glPushMatrix();
    glPopMatrix();
    //prism end
    glPushMatrix();
    glTranslatef(-10.0, 0.0f, -3.8);
    glRotatef(-90, 0, 0, 1);
    glColor3ub(14, 0, 215);
    triangularPrism(0.1, -1.0, 1.3);
    glPushMatrix();
    glPopMatrix();
    // position light left
    glPushMatrix();
    glTranslatef(11.0f, 0.0f, -5.0f);
    Plane::renderPosLightLeft();
    glPushMatrix();
    glPopMatrix();
    // aileron rudder left
    glPushMatrix();
    glRotatef(90, 1, 0, 0);
    glTranslatef(5.5f, -4.8f, 0.0f);
    glRotatef(aileron_left, 1, 0, 0);
    glColor3ub(215, 0, 0);
    Plane::renderWing(9.0f, 0.5f, 0.1f);
    glPushMatrix();
    glPopMatrix();
    //prism landing gears
    glPushMatrix();
    glTranslatef(-6.0, -0.35f, -3.8);
    glRotatef(-90, 0, 1, 0);
    glRotatef(90, 1, 0, 0);
    glColor3ub(215, 0, 0);
    triangularPrism(0.3, 0.3, 0.3);
    glPushMatrix();
    glPopMatrix();
}

/**
 * renders part of graph - tail
 */
void Plane::renderRear() {
    //new Graph
    glPushMatrix();
    //wing back node
    glPushMatrix();
    glRotatef(90, 1, 0, 0);
    glTranslatef(0.0f, -12.5f, -0.9f);
    glColor3ub(196, 196, 196);
    Plane::renderWing(5.0f, 2.0f, 0.2f);
    glPushMatrix();
    glPopMatrix();

    //front prism right
    glPushMatrix();
    glTranslatef(-1.0, 0.9f, -11.0);
    glRotatef(90, 0, 0, 1);
    glColor3ub(14, 0, 215);
    glColor3ub(196, 196, 196);
    triangularPrism(0.1, 1.5, 0.5);
    glPushMatrix();
    glPopMatrix();

    //end prism right
    glPushMatrix();
    glTranslatef(-2.5, 0.9f, -12.7);
    glRotatef(90, 0, 0, 1);
    glColor3ub(14, 0, 215);
    glColor3ub(196, 196, 196);
    triangularPrism(0.1, 0.7, 1.2);
    glPushMatrix();
    glPopMatrix();

    //front prism left
    glPushMatrix();
    glTranslatef(1.0, 0.9f, -11.0);
    glRotatef(-90, 0, 0, 1);
    glColor3ub(14, 0, 215);
    glColor3ub(196, 196, 196);
    triangularPrism(0.1, 1.5, 0.5);
    glPushMatrix();
    glPopMatrix();

    //end prism left
    glPushMatrix();
    glTranslatef(2.5, 0.9f, -12.7);
    glRotatef(-90, 0, 0, 1);
    glColor3ub(14, 0, 215);
    glColor3ub(196, 196, 196);
    triangularPrism(0.1, 0.7, 1.2);
    glPushMatrix();
    glPopMatrix();

    //elevator rudder back node
    glPushMatrix();
    glRotatef(90, 1, 0, 0);
    glTranslatef(0.0f, -13.7f, -0.9f);
    glRotatef(elevator, 1, 0, 0);
    glColor3ub(215, 0, 0);
    Plane::renderWing(5.0f, 0.5f, 0.1f);
    glPushMatrix();
    glPopMatrix();

    //prism tail
    glPushMatrix();
    glTranslatef(0.0, 1.0f, -11.5f);
    glColor3ub(14, 0, 215);
    triangularPrism(0.15, 2.5, 1.0);
    glPushMatrix();
    glPopMatrix();

    //polygon tail
    glPushMatrix();
    glTranslatef(0.0f, 2.0f, -12.7f);
    glColor3ub(14, 0, 215);
    Plane::renderWing(0.28f, 3.0f, 0.4f);
    glPushMatrix();
    glPopMatrix();

    //rudder tail
    glPushMatrix();
    glTranslatef(0.0f, 2.3f, -13.2f);
    glRotatef(rudder, 0, 1, 0);
    glColor3ub(215, 0, 0);
    Plane::renderWing(0.1f, 2.4f, 0.5f);
    glPushMatrix();
    glPopMatrix();

    //prism top tail
    glPushMatrix();
    glTranslatef(0.0f, 3.5f, -13.0f);
    glColor3ub(14, 0, 215);
    triangularPrism(0.15f, 0.3f, 0.5f);
    glPushMatrix();
    glPopMatrix();

    //position light
    glPushMatrix();
    glTranslatef(0.0f, 3.6f, -12.9f);
    Plane::renderPosLightBack();
    glPushMatrix();
    glPopMatrix();
}

/**
 * renders part of graph - body
 */
void Plane::renderBody() {
    //body
    glPushMatrix();
    glRotatef(90, 0, 1, 0);
    glTranslatef(5.0f, 0.0f, -2.0f);
    renderPlaneBody(8.0f, 1.0f, 1.0f);
    glPopMatrix();
    //cockpit
    glPushMatrix();
    glRotatef(90, 0, 1, 0);
    glTranslated(1.0f, 1.5, -1.75);
    Plane::renderCockpit(1.5, 0.5, 1);
    glPushMatrix();
    glPopMatrix();
    //plane body bottom
    glPushMatrix();
    glRotatef(90, 0, 1, 0);
    glTranslated(4.5f, -1.3, -2);
    Plane::renderPlaneBottom(7.5, 0.3, 1);
    glPushMatrix();
    glPopMatrix();
    //plane top
    glPushMatrix();
    glRotatef(90, 0, 1, 0);
    glTranslated(6.0f, 1.5, -2);
    Plane::renderPlaneTop(4.5, 0.5, 1);
    glPushMatrix();
    glPopMatrix();
}

/**
 * renders part of graph - propeller
 */
void Plane::renderPropeller() {
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, planeTexture[1]);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    GLUquadricObj *quadObj = gluNewQuadric();
    gluQuadricTexture(quadObj, GLU_TRUE);
    //glBindTexture(GL_TEXTURE_2D, planeTexture[1]);
    //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    //rotate propeller
    glRotated(90, 1, 0, 0);
    glRotatef(_angle, 0.0f, 1.0f, 0.0f);
    glTranslatef(0.0f, 3.5f, -2.0f);
    //blade 1
    glPushMatrix();
    glTranslated(-2.0f, -0.2f, 2.0f);
    Plane::renderPropellerBlade(5.0f, 0.2f, 0.3f);
    glPopMatrix();
    //blade 2
    glPushMatrix();
    glTranslated(+2.0f, -0.2f, 2.0f);
    Plane::renderPropellerBlade(5.0f, 0.2f, 0.3f);
    glPopMatrix();
    //blade 3
    glPushMatrix();
    glTranslated(0.0f, -0.2f, 4.0f);
    glRotatef(90, 0, 1, 0);
    Plane::renderPropellerBlade(5.0f, 0.2f, 0.3f);
    glPopMatrix();
    //blade 4
    glPushMatrix();
    glTranslated(0.0f, -0.2f, -0.0f);
    glRotatef(90, 0, 1, 0);
    Plane::renderPropellerBlade(5.0f, 0.2f, 0.3f);
    glPopMatrix();
    //center unit propeller
    glPushMatrix();
    glRotatef(-90, 1, 0, 0);
    glTranslated(0, -2, -0.5);
    glColor3f(1.0f, 1.0f, 1.0f);
    /*glTexCoord2f(1.0f, 0.0f);
    glutSolidCone(0.9, 1.5, 10, 10);*/
    gluCylinder(quadObj, 0.9, 0, 1.5, 10, 10);
    glPopMatrix();
    glDisable(GL_TEXTURE_2D);
}

void Plane::renderPosLightLeft() {
    if (toggle) {
        glColor3ub(255, 0, 0);
    } else {
        glColor3ub(255, 255, 255);
    }
    renderSphere(0.1);
}

void Plane::renderPosLightRight() {
    if (!toggle) {
        glColor3ub(0, 255, 0);

    } else {
        glColor3ub(255, 255, 255);
    }
    renderSphere(0.1);
}

void Plane::renderPosLightBack() {
    if (!toggle) {
        glColor3ub(255, 255, 255);

    } else {
        glColor3ub(14, 0, 215);
    }
    renderSphere(0.1);
}

void Plane::renderSphere(float radius) {
    GLUquadricObj *quadobj = gluNewQuadric();
    glEnable(GL_NORMALIZE);
    gluSphere(quadobj, radius, 10, 10);
}

void Plane::LandingGearUpAndDown() {
    //down
    if (Interface::landingGearToggle) {

        if (_angle_L_GearBackDoor > 90.0) {// stop door
            _angle_L_GearBackDoor -= 0.5;
        }

        if (_angle_L_GearsLeft > -90.0) {// stop gears left and right
            _angle_L_GearsRight += 0.2;
            _angle_L_GearsLeft -= 0.2;
        }

        if (_angle_L_GearBackDoor == 90.0) {// if open door
            if (driver_L_GearBackAndForth > -12.5) {// stop driver
                driver_L_GearBackAndForth -= 0.05;
            }
        }

        if (driver_L_GearBackAndForth > -12.6 && driver_L_GearBackAndForth < -12.4) {// if driver is ready
            if (_angle_L_GearBackRotation < 90.0) {// stop rotation wheel
                _angle_L_GearBackRotation += 0.5;
            }
        }

        if (_angle_L_GearBackRotation == 90.0) {// if rotation ready
            if (_angle_L_GearBackDownUp < 45.0) {// stop moving down
                _angle_L_GearBackDownUp += 0.2;
            } else {
                _angle_L_GearBackDownUp = 45.0;
            }
        }

    } else {
        if (_angle_L_GearBackDownUp > 0.2) { // stop moving down
            _angle_L_GearBackDownUp -= 0.2;
        } else {
            _angle_L_GearBackDownUp = 0.0;
        }

        if (_angle_L_GearBackDownUp == 0.0) { // if rotation ready
            if (_angle_L_GearBackRotation > 0.0) {
                _angle_L_GearBackRotation -= 0.5;
            }
        }

        if (_angle_L_GearBackRotation == 0.0) {
            if (driver_L_GearBackAndForth < -9.5) {// stop driver
                driver_L_GearBackAndForth += 0.05;
            }
        }

        if (_angle_L_GearsLeft < 90.0) {// stop gears left and right
            _angle_L_GearsRight -= 0.2;
            _angle_L_GearsLeft += 0.2;
        }

        if (_angle_L_GearsLeft >= 0.0) {// stop side gears
            _angle_L_GearsLeft = 0.0;
            _angle_L_GearsRight = 0.0;

        }

        if (driver_L_GearBackAndForth > -9.6 && driver_L_GearBackAndForth < -9.4) {//stop driver
            if (_angle_L_GearBackDoor < 120.0) {// stop door
                _angle_L_GearBackDoor += 0.5;
            }
        }
    }
}

/**
 * Here we return the general move by checking gears button
 */
float Plane::speed() {
    float planeCurrentSpeed;

    if (!Interface::engineOnOff) {
        if (gear > 0) {
            gear--;
        }
    }

    if (gear < gears.size()) {
        planeCurrentSpeed = gears.at(gear);
    } else {
        planeCurrentSpeed = gears.at(gear);
    }
    return planeCurrentSpeed;
}

/**
 * Here we trigger the plane movement up and down
 */
void Plane::moveUpDown(float circle) {
    if (circle == 0) {
        Interface::planeMoveY += speed() * 0;
    } else if (circle > 0 && circle <= 30) {
        Interface::planeMoveY += speed() * 0.15;
    } else if (circle <= 60 && circle > 30) {
        Interface::planeMoveY += speed() * 0.33;
    } else if (circle > 60 && circle <= 90) {
        Interface::planeMoveY += speed() * 0.66;
    } else if (circle == 90) {
        Interface::planeMoveY += speed() * 1;
    } else if (circle > 90 && circle <= 120) {
        Interface::planeMoveY += speed() * 0.66;
    } else if (circle > 120 && circle <= 150) {
        Interface::planeMoveY += speed() * 0.33;
    } else if (circle > 150 && circle <= 180) {
        Interface::planeMoveY += speed() * 0.15;
    } else if (circle == 180) {
        Interface::planeMoveY -= speed() * 0;
    } else if (circle > 180 && circle <= 210) {
        Interface::planeMoveY -= speed() * 0.15;
    } else if (circle > 210 && circle <= 240) {
        Interface::planeMoveY -= speed() * 0.33 * 2;
    } else if (circle > 240 && circle <= 270) {
        Interface::planeMoveY -= speed() * 0.66 * 4;
    } else if (circle == 270) {
        Interface::planeMoveY -= speed() * 1;
    } else if (circle > 270 && circle <= 300) {
        Interface::planeMoveY -= speed() * 0.66 * 4;
    } else if (circle > 300 && circle <= 330) {
        Interface::planeMoveY -= speed() * 0.33 * 2;
    } else if (circle > 330 && circle <= 359) {
        Interface::planeMoveY -= speed() * 0.15;
    }
}

/**
 * Here we trigger the plane movement left and right
 */
void Plane::moveLeftRight(float circle) {

    if (circle > 0 && circle <= 30) {
        Interface::planeMoveX += speed() * 0.15;
    } else if (circle <= 60 && circle > 30) {
        Interface::planeMoveX += speed() * 0.33;
    } else if (circle > 60 && circle <= 90) {
        Interface::planeMoveX += speed() * 0.66;
    } else if (circle == 90) {
        Interface::planeMoveX += speed() * 1;
    } else if (circle > 90 && circle <= 120) {
        Interface::planeMoveX += speed() * 0.66;
    } else if (circle > 120 && circle <= 150) {
        Interface::planeMoveX += speed() * 0.33;
    } else if (circle > 150 && circle <= 180) {
        Interface::planeMoveX += speed() * 0.15;
    } else if (circle == 180) {
        Interface::planeMoveX += 0;
    } else if (circle > 180 && circle <= 210) {
        Interface::planeMoveX -= speed() * 0.33;
    } else if (circle > 210 && circle <= 240) {
        Interface::planeMoveX -= speed() * 0.66;
    } else if (circle > 240 && circle <= 270) {
        Interface::planeMoveX -= speed() * 1;
    } else if (circle > 270 && circle <= 300) {
        Interface::planeMoveX -= speed() * 0.66;
    } else if (circle > 300 && circle <= 330) {
        Interface::planeMoveX -= speed() * 0.33;
    } else if (circle > 330 && circle <= 359) {
        Interface::planeMoveX -= speed() * 0.15;
    }
}

/**
 * Here we trigger the plane movement back- and forward
 */
void Plane::moveForwardBackword(float circle) {

    if (circle == 0 && (pitchDegree < 90 && pitchDegree > -90)) {
        Interface::planeMoveZ -= speed() * 1;
    } else if (circle > 0 && circle <= 30) {
        Interface::planeMoveZ -= speed() * 1;
    } else if (circle <= 60 && circle > 30) {
        Interface::planeMoveZ -= speed() * 0.33;
    } else if (circle > 60 && circle <= 90) {
        Interface::planeMoveZ -= speed() * 0.15;
    } else if (circle == 90) {
        Interface::planeMoveZ -= speed() * 0;
    } else if (circle > 90 && circle <= 120) {
        Interface::planeMoveZ += speed() * 0.15;
    } else if (circle > 120 && circle <= 150) {
        Interface::planeMoveZ += speed() * 0.33;
    } else if (circle > 150 && circle <= 180) {
        Interface::planeMoveZ += speed() * 0.66;
    } else if (circle == 180) {
        Interface::planeMoveZ += speed() * 1;
    } else if (circle > 180 && circle <= 210) {
        Interface::planeMoveZ += speed() * 0.66;
    } else if (circle > 210 && circle <= 240) {
        Interface::planeMoveZ += speed() * 0.33;
    } else if (circle > 240 && circle <= 270) {
        Interface::planeMoveZ -= speed() * 0.15;
    } else if (circle == 270) {
        Interface::planeMoveZ -= 0;
    } else if (circle > 270 && circle <= 300) {
        Interface::planeMoveZ -= speed() * 0.15;
    } else if (circle > 300 && circle <= 330) {
        Interface::planeMoveZ -= speed() * 0.33;
    } else if (circle > 330 && circle <= 359) {
        Interface::planeMoveZ -= speed() * 1;
    }
}

/**
 * Here we manipulate plane coordinates by checking degrees to let the plane move in space
 */
void Plane::fly() {
    const int CIRCLE = 360;

    //Fly up
    if (pitchDegree < 0 && ((abs(roleDegree) < 80) || roleDegree == 0 || abs(roleDegree) > 270)) {
        moveUpDown(abs(pitchDegree));
        moveForwardBackword(abs(pitchDegree));
        Interface::camAnglePitch = abs(pitchDegree);
        //Fly down
    } else if (pitchDegree > 0 || (pitchDegree < 0 && (abs(roleDegree) > 80 && abs(roleDegree) < 270))) {
        moveUpDown(CIRCLE - pitchDegree);
        moveForwardBackword(CIRCLE - pitchDegree);
        Interface::camAnglePitch = CIRCLE - pitchDegree;
    } else if (abs(yawDegree) < 90 || abs(yawDegree) > 270) {
        Interface::planeMoveZ -= speed();
    }

    if (yawDegree <= 0 && ((abs(roleDegree) < 80) || roleDegree == 0 || abs(roleDegree) > 270)) {
        moveLeftRight(abs(yawDegree));
        moveForwardBackword(abs(yawDegree));
        Interface::camAngleYaw = abs(yawDegree);
        //Fly down
    } else if (yawDegree > 0 || (pitchDegree < 0 && (abs(roleDegree) > 80 && abs(roleDegree) < 270))) {
        moveLeftRight(CIRCLE - yawDegree);
        moveForwardBackword(CIRCLE - yawDegree);
        Interface::camAngleYaw = CIRCLE - yawDegree;
    }
}

void Plane::resetAngle() {
    if (abs(roleDegree) >= 360) {
        roleDegree = 0;
    }
    if (abs(pitchDegree) >= 360) {
        pitchDegree = 0;
    }
    if (abs(yawDegree) >= 360) {
        yawDegree = 0;
    }
}
/**
 * Here we provide a limitation for our plane in space
 */
void Plane::limitFieldPosition() {
    //front limit
    if (Interface::planeMoveZ <= -10000) {
        Interface::planeMoveZ = -1;
    }

    //Back limit
    if (Interface::planeMoveZ > 0) {
        Interface::planeMoveZ = -1;
    }
    //top limit
    if (Interface::planeMoveY >= 5000) {
        Interface::planeMoveY = 5000;
    }

    //bottom limit (crash)
    if (!Interface::landingGearToggle) {
        if (Interface::planeMoveY <= 1) {
            Interface::planeMoveY = 1000;
            Interface::planeMoveZ = -1;
            Interface::planeMoveX = 0;
            Plane::pitchDegree = 0;
            Plane::yawDegree = 0;
            Plane::roleDegree = 0;
            Interface::viewOnBackCam();
        }
    } else {
        if (Interface::planeMoveY <= 10) {
            Interface::planeMoveY = 10;
        }
    }
    //left
    if (Interface::planeMoveX <= -5000) {
        Interface::planeMoveX = -5000;
    }
    //right
    if (Interface::planeMoveX >= 5000) {
        Interface::planeMoveX = 5000;
    }

    //reload
    if (Interface::planeMoveZ >= 9999 || Interface::planeMoveY <= 1) {
        Environment::setRandMapType();
    }

    //engine off
    if (gears.at(gear) < 1) {
        Interface::planeMoveY -= 0.5;
        if (Interface::planeMoveY > 10) {
            Interface::planeMoveZ -= 0.5 * gears.at(gear);
        }
    }
}

/**
 * Here we trigger the load of images and store textures
 */
void Plane::loadTextures() {
    pushTexture("textures/planeBody.bmp");
    pushTexture("textures/propellerTip.bmp");
}

/**
 * Here we load an image and store it as texture
 */
void Plane::pushTexture(const char *file) {
    GLuint texture;
    Image *image = loadBMP(file);
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, image->width, image->height, 0, GL_RGB, GL_UNSIGNED_BYTE, image->pixels);
    delete image;
    planeTexture.push_back(texture);
}

void Plane::triangularPrism(float x, float y, float z) {
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, planeTexture[0]);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glBegin(GL_QUADS);
    glNormal3f(0.0f, 0.0f, -1.0f);
    glTexCoord2f(0.0f, 0.0f);
    glVertex3f(x, 0, z);
    glTexCoord2f(1.0f, 0.0f);
    glVertex3f(x, 0, -z);
    glTexCoord2f(1.0f, 1.0f);
    glVertex3f(-x, 0, -z);
    glTexCoord2f(0.0f, 1.0f);
    glVertex3f(-x, 0, z);
    glEnd();

    glBegin(GL_QUADS);

    glTexCoord2f(0.0f, 0.0f);
    glVertex3f(x, 0, -z);
    glTexCoord2f(1.0f, 0.0f);
    glVertex3f(x, y, -z);
    glTexCoord2f(1.0f, 1.0f);
    glVertex3f(-x, y, -z);
    glTexCoord2f(0.0f, 1.0f);
    glVertex3f(-x, 0, -z);
    glEnd();

    glBegin(GL_QUADS);//top
    glNormal3f(0.0f, 1.0f, 0.0f);
    glTexCoord2f(0.0f, 0.0f);
    glVertex3f(x, y, -z);
    glTexCoord2f(1.0f, 0.0f);
    glVertex3f(-x, y, -z);
    glTexCoord2f(1.0f, 1.0f);
    glVertex3f(-x, 0, z);
    glTexCoord2f(0.0f, 1.0f);
    glVertex3f(x, 0, z);
    glEnd();

    glBegin(GL_TRIANGLES);
    glNormal3f(1.0f, 0.0f, 0.0f);
    glTexCoord2f(0.0f, 0.0f);
    glVertex3f(x, 0, z);
    glTexCoord2f(1.0f, 0.0f);
    glVertex3f(x, y, -z);
    glTexCoord2f(1.0f, 1.0f);
    glVertex3f(x, 0, -z);
    glEnd();

    glBegin(GL_TRIANGLES);
    glNormal3f(-1.0f, 0.0f, 0.0f);
    glTexCoord2f(0.0f, 0.0f);
    glVertex3f(-x, 0, z);
    glTexCoord2f(1.0f, 0.0f);
    glVertex3f(-x, y, -z);
    glTexCoord2f(1.0f, 1.0f);
    glVertex3f(-x, 0, -z);
    glEnd();

    glDisable(GL_TEXTURE_2D);
}




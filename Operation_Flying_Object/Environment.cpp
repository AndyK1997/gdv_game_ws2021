#include "Environment.h"

std::vector<GLuint> texturen;
int *Environment::arrayMapTypes = new int[(int) (groundZ * groundX)];
Cloud *Environment::cloudTypes = new Cloud[(int) (groundX * groundZ)];

/**
 * here we create the sun
 */
void Environment::renderSun() {
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_COLOR_MATERIAL);
    // z-buffer
    glEnable(GL_DEPTH_TEST);
    glClearDepth(1.0);
    // calculate light and normalize
    glEnable(GL_NORMALIZE);

    glBindTexture(GL_TEXTURE_2D, texturen[7]);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    GLUquadricObj *quadobj = gluNewQuadric();
    gluQuadricTexture(quadobj, GLU_TRUE);
    gluQuadricNormals(quadobj, GLU_SMOOTH);
    glTranslated(-1000, 2000.0, Interface::planeMoveZ-10000);
    glColor3ub(255, 255, 102);
    gluSphere(quadobj, 300, 200, 200);
    glDisable(GL_TEXTURE_2D);
}

/**
 * here we create the sky
 */
void Environment::renderSky() {
    /*glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texturen[6]);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);*/
    glColor3ub(10, 149, 255);

    // horizon
    glBegin(GL_POLYGON);
    glTexCoord2f(0.0f, 0.0f);
    glVertex3f(-groundX, groundZ, -groundZ);
    glTexCoord2f(1.0f, 0.0f);
    glVertex3f(-groundX, 1.0, -groundZ);
    glTexCoord2f(1.0f, 1.0f);
    glVertex3f(groundX, 1.0, -groundZ);
    glTexCoord2f(0.0f, 1.0f);
    glVertex3f(groundX, groundZ, -groundZ);
    glEnd();

    glBegin(GL_POLYGON);
    glTexCoord2f(0.0f, 0.0f);
    glVertex3f(-groundX, groundZ, -groundZ);
    glTexCoord2f(1.0f, 0.0f);
    glVertex3f(-groundX, groundZ, 0);
    glTexCoord2f(1.0f, 1.0f);
    glVertex3f(groundX, groundZ, 0);
    glTexCoord2f(0.0f, 1.0f);
    glVertex3f(groundX, groundZ, -groundZ);
    glEnd();

    glBegin(GL_POLYGON);
    glTexCoord2f(0.0f, 0.0f);
    glVertex3f(-groundX, groundZ, -groundZ);
    glTexCoord2f(1.0f, 0.0f);
    glVertex3f(-groundX, 1.0, -groundZ);
    glTexCoord2f(1.0f, 1.0f);
    glVertex3f(-groundX, 1.0, 0);
    glTexCoord2f(0.0f, 1.0f);
    glVertex3f(-groundX, groundZ, 0);
    glEnd();

    glBegin(GL_POLYGON);
    glVertex3f(-groundX, groundZ, -groundZ);
    glVertex3f(-groundX, 1.0, -groundZ);
    glVertex3f(-groundX, 1.0, 0);
    glVertex3f(-groundX, groundZ, 0);
    glEnd();

    glBegin(GL_POLYGON);
    glVertex3f(groundX, groundZ, -groundZ);
    glVertex3f(groundX, 1.0, -groundZ);
    glVertex3f(groundX, 1.0, 0);
    glVertex3f(groundX, groundZ, 0);
    glEnd();

    glBegin(GL_POLYGON);
    glVertex3f(-groundX, groundZ, groundZ);
    glVertex3f(-groundX, 1.0, groundZ);
    glVertex3f(groundX, 1.0, groundZ);
    glVertex3f(groundX, groundZ, groundZ);
    glEnd();
    glDisable(GL_TEXTURE_2D);
}

/**
 * here we create a field with a texture
 */
void Environment::createField(float xOld, float xNew, float y, float z, GLuint texture) {
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    //glColor3ub(14,142,48);
    glBegin(GL_QUADS);
    glTexCoord2f(0.0f, 0.0f);
    glVertex3f(xOld, y, -z);
    glTexCoord2f(1.0f, 0.0f);
    glVertex3f(xOld, y, -z + FIELDSIZE);
    glTexCoord2f(1.0f, 1.0f);
    glVertex3f(xNew, y, -z + FIELDSIZE);
    glTexCoord2f(0.0f, 1.0f);
    glVertex3f(xNew, y, -z);
    glEnd();
    glDisable(GL_TEXTURE_2D);
}

/**
 * here we generate a random field from type wood
 */
void Environment::createWood(float xOld, float xNew, float y, float z) {
    createField(xOld, xNew, y, z, texturen[2]);

    const int MAX_COUNT_OF_TREE = 10;
    for (unsigned int i = 0; i < MAX_COUNT_OF_TREE; i++) {
        // renderTree();
    }
}

/**
 * here we generate a random field from type arce
 */
 void Environment::createAcre(float xOld, float xNew, float y, float z) {
    createField(xOld, xNew, y, z, texturen[4]);
}

/**
 * here we generate a random field from type cultivated field
 */
void Environment::createCultivatedField(float xOld, float xNew, float y, float z) {
    createField(xOld, xNew, y, z, texturen[5]);
}

/**
 * here we generate a random field from type meadow
 */
void Environment::createMeadow(float xOld, float xNew, float y, float z) {
    createField(xOld, xNew, y, z, texturen[1]);
}

/**
 * here we generate a random field from type city
 */
void Environment::createCity(float xOld, float xNew, float y, float z) {
    createField(xOld, xNew, y, z, texturen[3]);
    /*int grid=FIELDSIZE/4;
    for(int x=xOld;x<xNew; x++){
        for(int i=0; i<FIELDSIZE;i++){
            if(x%grid==0 && i%grid==0){
                renderHouse(x,y,i);
            }
        }
    }*/
}

/**
 * here we generate a random field from type sea
 */
void Environment::createSea(float xOld, float xNew, float y, float z) {
    createField(xOld, xNew, y, z, texturen[0]);
}

/**
 * here we place the random map fields on the map
 */
void Environment::placeFields() {
    glColor3ub(255, 255, 255);
    int arrayMapTypesIndex = 0;
    for (int z = FIELDSIZE; z < groundZ; z += FIELDSIZE) {
        for (int x = -groundX; x < groundX; x += FIELDSIZE) {
            //int fieldSize=(rand()%Size)*(1/partitioning);

            switch (arrayMapTypes[arrayMapTypesIndex]) {
                case 0:
                    createMeadow(x, x + FIELDSIZE, 1.0, z);
                    break;
                case 1:
                    createAcre(x, x + FIELDSIZE, 1.0, z);
                    break;
                case 2:
                    createWood(x, x + FIELDSIZE, 1.0, z);
                    break;
                case 3:
                    createCultivatedField(x, x + FIELDSIZE, 1.0, z);
                    break;
                case 4:
                    createSea(x, x + FIELDSIZE, 1.0, z);
                    break;
                case 5:
                    createCity(x, x + FIELDSIZE, 1.0, z);
                    break;
            }
            arrayMapTypesIndex++;
        }
        //arrayMapTypesIndex++;
    }
}

/**
 * here generate a random map type
 */
void Environment::setRandMapType() {
    for (unsigned int x = 0; x < (int) (groundZ * groundX); x++) {
        arrayMapTypes[x] = rand() % 6;
        cloudTypes[x].type = rand() % 4;
        cloudTypes[x].high = 1500 + rand() % 1700;
        cloudTypes[x].widthOffset = rand() % 1500;
        cloudTypes[x].deepOffset = 50 + rand() % 450;
        //std::cout<<cloudTypes[x].high<<std::endl;
    }
}

/**
 * here we set random values to place and generate our clouds
 */
void Environment::createRandValuesForClouds() {
    for (unsigned int x = 0; x < (int) (groundZ * groundX); x++) {
        cloudTypes[x].type = rand() % 4;
        cloudTypes[x].high = 1500 + rand() % 1700;
        cloudTypes[x].widthOffset = rand() % 1500;
        cloudTypes[x].deepOffset = 50 + rand() % 450;
        //std::cout<<cloudTypes[x].high<<std::endl;
    }
}

/**
 * here we place the clouds in random order
 */
void Environment::renderClouds() {
    int arrayMapTypesIndex = 0;
    for (int z = FIELDSIZE; z < groundZ; z += FIELDSIZE) {
        for (int x = -groundX; x < groundX; x += FIELDSIZE) {
            switch (cloudTypes[arrayMapTypesIndex].type) {
                case 0:
                    renderCloud(x, x + cloudTypes[arrayMapTypesIndex].widthOffset, cloudTypes[arrayMapTypesIndex].high,
                                z / 2, cloudTypes[arrayMapTypesIndex].deepOffset);
                    break;
                case 1:
                    renderCloud(x, x + cloudTypes[arrayMapTypesIndex].widthOffset, cloudTypes[arrayMapTypesIndex].high,
                                z / 3, cloudTypes[arrayMapTypesIndex].deepOffset);
                    break;
                case 2:
                    renderCloud(x, x + cloudTypes[arrayMapTypesIndex].widthOffset, cloudTypes[arrayMapTypesIndex].high,
                                z / 4, cloudTypes[arrayMapTypesIndex].deepOffset);
                    break;
                case 3:
                    renderCloud(x, x + cloudTypes[arrayMapTypesIndex].widthOffset, cloudTypes[arrayMapTypesIndex].high,
                                z / 1, cloudTypes[arrayMapTypesIndex].deepOffset);
                    break;
            }
            arrayMapTypesIndex++;
        }
    }
}

/**
 * Here we create one cloud
 */
void Environment::renderCloud(int xOld, int xNew, float y, int z, float deepness) {
    //glEnable( GL_BLEND );
    glColor4ub(255, 255, 255,1.0);
    float cloudThickness = 10;
    float stageOneOffset = +20;
    //Bottom
    glBegin(GL_QUADS);
    glNormal3f(0.0f, -1.0f, 0.0f);
    glVertex3f(xOld, y, -z);
    glVertex3f(xOld, y, -z + deepness);
    glVertex3f(xNew, y, -z + deepness);
    glVertex3f(xNew, y, -z);
    glEnd();

    //front
    glBegin(GL_QUADS);
    glNormal3f(0.0f, 0.0f, -1.0f);
    glVertex3f(xOld, y + cloudThickness, -z + deepness);
    glVertex3f(xOld, y, -z + deepness);
    glVertex3f(xNew, y, -z + deepness);
    glVertex3f(xNew, y + cloudThickness, -z + deepness);
    glEnd();

    //back
    glBegin(GL_QUADS);
    glNormal3f(0.0f, 0.0f, 1.0f);
    glVertex3f(xOld, y + cloudThickness, -z);
    glVertex3f(xOld, y, -z);
    glVertex3f(xNew, y, -z);
    glVertex3f(xNew, y + cloudThickness, -z);
    glEnd();

    //top
    glBegin(GL_QUADS);
    glNormal3f(0.0f, 1.0f, 0.0f);
    glVertex3f(xOld, y + cloudThickness, -z);
    glVertex3f(xOld, y + cloudThickness, -z + deepness);
    glVertex3f(xNew, y + cloudThickness, -z + deepness);
    glVertex3f(xNew, y + cloudThickness, -z);
    glEnd();

    //left
    glBegin(GL_QUADS);
    glNormal3f(-1.0f, 0.0f, 0.0f);
    glVertex3f(xOld, y + cloudThickness, -z);
    glVertex3f(xOld, y, -z);
    glVertex3f(xOld, y, -z + deepness);
    glVertex3f(xOld, y + cloudThickness, -z + deepness);
    glEnd();

    //right
    glBegin(GL_QUADS);
    glNormal3f(1.0f, 0.0f, 0.0f);
    glVertex3f(xNew, y + cloudThickness, -z);
    glVertex3f(xNew, y, -z);
    glVertex3f(xNew, y, -z + deepness);
    glVertex3f(xNew, y + cloudThickness, -z + deepness);
    glEnd();

    //Stage 2
    //Bottom
    glBegin(GL_QUADS);
    glNormal3f(0.0f, -1.0f, 0.0f);
    glVertex3f(xOld+stageOneOffset, y + cloudThickness, -z-stageOneOffset);
    glVertex3f(xOld+stageOneOffset, y + cloudThickness, -z + deepness-stageOneOffset);
    glVertex3f(xNew+stageOneOffset, y + cloudThickness, -z + deepness-stageOneOffset);
    glVertex3f(xNew+stageOneOffset, y + cloudThickness, -z-stageOneOffset);
    glEnd();

    //FRont
    glBegin(GL_QUADS);
    glNormal3f(0.0f, 0.0f, -1.0f);
    glVertex3f(xOld+stageOneOffset, y + cloudThickness * 2, -z + deepness-stageOneOffset);
    glVertex3f(xOld+stageOneOffset, y + cloudThickness, -z + deepness-stageOneOffset);
    glVertex3f(xNew+stageOneOffset, y + cloudThickness, -z + deepness-stageOneOffset);
    glVertex3f(xNew+stageOneOffset, y + cloudThickness * 2, -z + deepness-stageOneOffset);
    glEnd();

    //Back
    glBegin(GL_QUADS);
    glNormal3f(0.0f, 0.0f, 1.0f);
    glVertex3f(xOld+stageOneOffset, y + cloudThickness * 2, -z-stageOneOffset);
    glVertex3f(xOld+stageOneOffset, y + cloudThickness, -z-stageOneOffset);
    glVertex3f(xNew+stageOneOffset, y + cloudThickness, -z-stageOneOffset);
    glVertex3f(xNew+stageOneOffset, y + cloudThickness * 2, -z-stageOneOffset);
    glEnd();

    //top
    glBegin(GL_QUADS);
    glNormal3f(0.0f, 1.0f, 0.0f);
    glVertex3f(xOld+stageOneOffset, y + cloudThickness * 2, -z-stageOneOffset);
    glVertex3f(xOld+stageOneOffset, y + cloudThickness * 2, -z + deepness-stageOneOffset);
    glVertex3f(xNew+stageOneOffset, y + cloudThickness * 2, -z + deepness-stageOneOffset);
    glVertex3f(xNew+stageOneOffset, y + cloudThickness * 2, -z-stageOneOffset);
    glEnd();

    //left
    glBegin(GL_QUADS);
    glNormal3f(-1.0f, 0.0f, 0.0f);
    glVertex3f(xOld+stageOneOffset, y + cloudThickness * 2, -z-stageOneOffset);
    glVertex3f(xOld+stageOneOffset, y + cloudThickness, -z-stageOneOffset);
    glVertex3f(xOld+stageOneOffset, y + cloudThickness, -z + deepness-stageOneOffset);
    glVertex3f(xOld+stageOneOffset, y + cloudThickness * 2, -z + deepness-stageOneOffset);
    glEnd();

    //right
    glBegin(GL_QUADS);
    glNormal3f(1.0f, 0.0f, 0.0f);
    glVertex3f(xNew+stageOneOffset, y + cloudThickness * 2, -z-stageOneOffset);
    glVertex3f(xNew+stageOneOffset, y + cloudThickness, -z-stageOneOffset);
    glVertex3f(xNew+stageOneOffset, y + cloudThickness, -z + deepness-stageOneOffset);
    glVertex3f(xNew+stageOneOffset, y + cloudThickness * 2, -z + deepness-stageOneOffset);
    glEnd();

    //Stage 3
    //Bottom
    glBegin(GL_QUADS);
    glNormal3f(0.0f, -1.0f, 0.0f);
    glVertex3f(xOld+stageOneOffset*2, y + cloudThickness, -z-stageOneOffset*2);
    glVertex3f(xOld+stageOneOffset*2, y + cloudThickness, -z + deepness-stageOneOffset*2);
    glVertex3f(xNew+stageOneOffset*2, y + cloudThickness, -z + deepness-stageOneOffset*2);
    glVertex3f(xNew+stageOneOffset*2, y + cloudThickness, -z-stageOneOffset*2);
    glEnd();

    //FRont
    glBegin(GL_QUADS);
    glNormal3f(0.0f, 0.0f, -1.0f);
    glVertex3f(xOld+stageOneOffset*2, y + cloudThickness * 3, -z + deepness-stageOneOffset*2);
    glVertex3f(xOld+stageOneOffset*2, y + cloudThickness * 2, -z + deepness-stageOneOffset*2);
    glVertex3f(xNew+stageOneOffset*2, y + cloudThickness * 2, -z + deepness-stageOneOffset*2);
    glVertex3f(xNew+stageOneOffset*2, y + cloudThickness * 3, -z + deepness-stageOneOffset*2);
    glEnd();

    //Back
    glBegin(GL_QUADS);
    glNormal3f(0.0f, 0.0f, 1.0f);
    glVertex3f(xOld+stageOneOffset*2, y + cloudThickness * 2, -z-stageOneOffset*2);
    glVertex3f(xOld+stageOneOffset*2, y + cloudThickness * 2, -z-stageOneOffset*2);
    glVertex3f(xNew+stageOneOffset*2, y + cloudThickness * 2, -z-stageOneOffset*2);
    glVertex3f(xNew+stageOneOffset*2, y + cloudThickness * 3, -z-stageOneOffset*2);
    glEnd();

    //top
    glBegin(GL_QUADS);
    glNormal3f(0.0f, 1.0f, 0.0f);
    glVertex3f(xOld+stageOneOffset*2, y + cloudThickness * 3, -z-stageOneOffset*2);
    glVertex3f(xOld+stageOneOffset*2, y + cloudThickness * 3, -z + deepness-stageOneOffset*2);
    glVertex3f(xNew+stageOneOffset*2, y + cloudThickness * 3, -z + deepness-stageOneOffset*2);
    glVertex3f(xNew+stageOneOffset*2, y + cloudThickness * 3, -z-stageOneOffset*2);
    glEnd();

    //left
    glBegin(GL_QUADS);
    glNormal3f(-1.0f, 0.0f, 0.0f);
    glVertex3f(xOld+stageOneOffset*2, y + cloudThickness * 3, -z-stageOneOffset*2);
    glVertex3f(xOld+stageOneOffset*2, y + cloudThickness * 2, -z-stageOneOffset*2);
    glVertex3f(xOld+stageOneOffset*2, y + cloudThickness * 2, -z + deepness-stageOneOffset*2);
    glVertex3f(xOld+stageOneOffset*2, y + cloudThickness * 3, -z + deepness-stageOneOffset*2);
    glEnd();

    //right
    glBegin(GL_QUADS);
    glNormal3f(1.0f, 0.0f, 0.0f);
    glVertex3f(xNew+stageOneOffset*2, y + cloudThickness * 3, -z-stageOneOffset*2);
    glVertex3f(xNew+stageOneOffset*2, y + cloudThickness * 2, -z-stageOneOffset*2);
    glVertex3f(xNew+stageOneOffset*2, y + cloudThickness * 2, -z + deepness-stageOneOffset*2);
    glVertex3f(xNew+stageOneOffset*2, y + cloudThickness * 3, -z + deepness-stageOneOffset*2);
    glEnd();
    glDisable( GL_BLEND );
}

void Environment::renderHouse(float x, float y, float z) {
    //bottom
    glColor3ub(255,0,0);
    glBegin(GL_QUADS);
    glTexCoord2f(0.0f, 0.0f);
    glVertex3f(x, y, -z);
    glTexCoord2f(1.0f, 0.0f);
    glVertex3f(x, y, -z + 0.5);
    glTexCoord2f(1.0f, 1.0f);
    glVertex3f(x-0.5, y, -z +0.5);
    glTexCoord2f(0.0f, 1.0f);
    glVertex3f(x-0.5, y, -z);
    glEnd();
}

/**
 * here we trigger the loading of textures
 */
void Environment::loadTextures() {
    pushTexture("textures/sea2.bmp");
    pushTexture("textures/grass2.bmp");
    pushTexture("textures/forest2.bmp");
    pushTexture("textures/city.bmp");
    pushTexture("textures/acre2.bmp");
    pushTexture("textures/field2.bmp");
    pushTexture("textures/sky.bmp");
    pushTexture("textures/sun.bmp");
    pushTexture("textures/moon.bmp");
}

/**
 * here we store our textures
 */
void Environment::pushTexture(const char *file) {
    GLuint texture;
    Image *image = loadBMP(file);
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, image->width, image->height, 0, GL_RGB, GL_UNSIGNED_BYTE, image->pixels);
    delete image;
    texturen.push_back(texture);
}
#ifndef OPERATION_FLYING_OBJECT_CHALLENGE_H
#define OPERATION_FLYING_OBJECT_CHALLENGE_H

#include <vector>
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <GL/glut.h>
#include "Interface.h"

using namespace std;

class Challenge {
public:
    void createRandomNodes();
    void createPath();

private:
    struct Node {
        float x;
        float y;
        float z;
        int color_red;
        int color_green;
        int color_blue;
        bool collision;
        float motion;
    };

    // starting point of challenge
    float pos_x = 0;
    float pos_y = 1000;
    float pos_z = -3000;

    float value = 20.0;

    const int max_nodes = 50;
    bool toggle = false;

    void checkCollision(int index);
    void triggerNode();
    void renderNode(int index);

    float randomValue[12] = {50.0, -50.0, 100.0, -100.0, 150.0, -150.0, 200.0, -200.0, 250.0, -250.0, 500.0, -500.0};
    vector<Node> route;
};

#endif //OPERATION_FLYING_OBJECT_CHALLENGE_H
